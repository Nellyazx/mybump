package com.pregman.pregman.ui.main.stocks;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pregman.pregman.data.model.StockInfo;
import com.pregman.pregman.data.sqlite.Constants;
import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.ui.main.stocks.single.StockViewActivity;
import com.stockbroker.stockbroker.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by NELSON-PC on 2/7/2018.
 */

public class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.StockViewHolder> {
    private Context context;
    MyDb myDb;
    public List<StockInfo> stockInfos;

    public StocksAdapter(Context context, List<StockInfo> stockInfos) {
        this.context = context;
        this.stockInfos = stockInfos;

    }

    @Override
    public StockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customcard, parent, false);
        StockViewHolder stockViewHolder = new StockViewHolder(view);
        return stockViewHolder;
    }

    @Override
    public void onBindViewHolder(final StockViewHolder holder, final int position) {
        final Context context = holder.itemView.getContext();

        myDb = new MyDb(context);

        holder.stockName.setText(stockInfos.get(position).getStockName());
        holder.stockToday.setText(new DecimalFormat("00.00").format(stockInfos.get(position).getStockPriceToday()));
        holder.stockYesterday.setText(new DecimalFormat("00.00").format(stockInfos.get(position).getStockPriceYesterDay()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, StockViewActivity.class);
                intent.putExtra("stockName", stockInfos.get(position).getStockName());
                intent.putExtra("stockToday", new DecimalFormat("00.00").format(stockInfos.get(position).getStockPriceToday()));
                intent.putExtra("stockYesterday", new DecimalFormat("00.00").format(stockInfos.get(position).getStockPriceYesterDay()));
                context.startActivity(intent);
            }
        });

        holder.addToWish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count = 0;
                String stockName = null;

                myDb.open();

                Cursor cl = myDb.getWishes();
                while (cl.moveToNext()) {
                    count = cl.getCount();
                    stockName = cl.getString(cl.getColumnIndex(Constants.STOCK_NAME));

                }
                cl.close();

                if (count < 20) {

//                    MyWish wish = new MyWish();
//
//                    wish.setStockName(holder.stockName.getText().toString());
//                    wish.setStockHigh(new DecimalFormat("00.00").format(stockInfo.getStockHigh()));
//                    wish.setStockVol(new DecimalFormat("00.00").format(stockInfo.getStockVol()));
//                    //wish.setStockVol();

                    if (stockInfos.get(position).getStockName().equals(stockName)) {
                        Toast.makeText(context, "Stock " + holder.stockName.getText().toString() + " already exists the Wish List", Toast.LENGTH_LONG).show();

                    } else {
                        myDb.insertWish(
                                holder.stockName.getText().toString(),
                                new DecimalFormat("00.00").format(stockInfos.get(position).getStockHigh()),
                                new DecimalFormat("00.00").format(stockInfos.get(position).getStockVol()));

                        myDb.close();

                        Toast.makeText(context, "Stock " + holder.stockName.getText().toString() + " was added To the Wish List", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, " You have reached the maximum limit", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (stockInfos != null) {
            return stockInfos.size();
        }
        return 0;
    }

    public void clearAdapter() {
        stockInfos.clear();
        notifyDataSetChanged();
    }

    //view holder class
    public static class StockViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public TextView stockName;
        public TextView stockYesterday;
        public TextView stockToday;
        public ImageView addToWish;

        public StockViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            stockName = (TextView) itemView.findViewById(R.id.StockName);
            stockYesterday = (TextView) itemView.findViewById(R.id.Yesterday);
            stockToday = (TextView) itemView.findViewById(R.id.Today);
            addToWish = (ImageView) itemView.findViewById(R.id.addToWish);

        }
    }
}
