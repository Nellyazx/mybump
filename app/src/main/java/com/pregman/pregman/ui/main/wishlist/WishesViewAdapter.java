package com.pregman.pregman.ui.main.wishlist;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pregman.pregman.data.model.MyWish;
import com.pregman.pregman.data.sqlite.MyDb;
import com.stockbroker.stockbroker.R;

import java.util.List;

/**
 * Created by Admin on 2/17/2018.
 */

public class WishesViewAdapter extends RecyclerView.Adapter<WishesViewAdapter.WishViewHolder> {
    private Context context;
    MyDb myDb;
    private List<MyWish> wishList;

    public WishesViewAdapter(Context context, List<MyWish> wishList) {
        this.context = context;
        this.wishList = wishList;

    }

    @Override
    public WishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.watchlist, parent, false);
        return new WishViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final WishViewHolder holder, final int position) {
        final Context context = holder.itemView.getContext();

        myDb =  new MyDb(context);

        holder.shareName.setText(wishList.get(position).getStockName());
        holder.shareHigh.setText(wishList.get(position).getStockHigh());
        holder.shareVol.setText(wishList.get(position).getStockVol());

        holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myDb.open();
                myDb.deleteWishByName(wishList.get(position).getStockName());
                myDb.close();

                wishList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, wishList.size());

//                Toast.makeText(mContext, "" + mCartList.get(position).getOrderId(), Toast.LENGTH_SHORT).show();

                myDb.close();


            }
        });
    }

    @Override
    public int getItemCount() {
        if (wishList != null) {
            return wishList.size();
        }
        return 0;
    }

    //view holder class
    public static class WishViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public TextView shareName;
        public TextView shareHigh;
        public TextView shareVol;
        public ImageView imgRemove;

        public WishViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.watchMyStock);
            shareName = (TextView) itemView.findViewById(R.id.shareName);
            shareVol = (TextView) itemView.findViewById(R.id.shareHigh);
            shareHigh = (TextView) itemView.findViewById(R.id.shareVol);
            imgRemove = (ImageView) itemView.findViewById(R.id.imgRemove);

        }
    }
}
