package com.pregman.pregman.ui.start;

import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.ui.auth.Login;
import com.pregman.pregman.ui.main.StocksActivity;
import com.stockbroker.stockbroker.R;

public class SplashActivity extends AppCompatActivity {

    MyDb myDb;
    int count =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        setContentView(R.layout.activity_splash);

        myDb = new MyDb(SplashActivity.this);

        myDb.open();
        Cursor cl = myDb.getUser();
        while (cl.moveToNext()) {
            count = cl.getCount();
        }
        cl.close();
        myDb.close();

        if (count == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, Login.class));
                }
            }, 2000);
        } else if (count != 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, StocksActivity.class));
                }
            }, 2000);
        }
    }

//    public void loadSlides(View view) {
//        new PreferenceManager(this).clearPreference();
//        startActivity(new Intent(this, WelcomeActivity.class));
//    }

}
