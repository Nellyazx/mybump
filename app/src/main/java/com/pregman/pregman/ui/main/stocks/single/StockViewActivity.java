package com.pregman.pregman.ui.main.stocks.single;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pregman.pregman.data.api.ApiService;
import com.pregman.pregman.data.model.Graph;
import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.utils.ConnectionDetector;
import com.pregman.pregman.utils.MySingleTone;
import com.stockbroker.stockbroker.R;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StockViewActivity extends AppCompatActivity {
    Boolean isInternetPresent = false;
    ConnectionDetector mConnectionDetector;
    MyDb myDb;
    LineChart mChart;
    private List<Graph> graphList;
    String stockName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_view);

        myDb = new MyDb(StockViewActivity.this);
        mConnectionDetector = new ConnectionDetector(StockViewActivity.this);
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        Intent intent = getIntent();
        stockName = intent.getStringExtra("stockName");
        String stockToday = intent.getStringExtra("stockToday");
        String stockYesterday = intent.getStringExtra("stockYesterday");

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(stockName);

        TextView Today = (TextView) findViewById(R.id.Today);
        Today.setText(stockToday);

        TextView Yesterday = (TextView) findViewById(R.id.Yesterday);
        Yesterday.setText(stockYesterday);

        graphList = new ArrayList<>();
        mChart = (LineChart) findViewById(R.id.lineChart);

        getData();

    }

    public void getData() {

        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        if (isInternetPresent) {

            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ApiService.GET_DATA_JSON, new Response.Listener<JSONArray>() {
                @SuppressLint("LogConditional")
                @Override
                public void onResponse(JSONArray response) {

                    Log.d("JSON DATA", response.toString());

                    if (response == null) {
                        Toast.makeText(StockViewActivity.this, "Couldn't fetch the stock data! Pleas try again.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    List<Graph> items = new Gson().fromJson(response.toString(), new TypeToken<List<Graph>>() {
                    }.getType());

                    graphList.clear();
                    graphList.addAll(items);
                    populateLeaveChart(items);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error in getting json
//                    Toast.makeText(StockViewActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

            MySingleTone.getInstance(StockViewActivity.this).addToRequestQueue(jsonArrayRequest);

        } else {
            Toast.makeText(StockViewActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy/MM/dd";
        String outputPattern = "dd/MMM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void populateLeaveChart(List<Graph> graphList) {
        ArrayList<Entry> yValues = new ArrayList<Entry>();

//        Toast.makeText(StockViewActivity.this, ""+graphList.size(), Toast.LENGTH_SHORT).show();

//        String stckName = null;

        for (int i = 0; i < graphList.size(); i++) {

//            stckName = graphList.get(i).getSecurity();

            if (graphList.get(i).getSecurity().equals(stockName)) {

                float yVal = Float.valueOf(graphList.get(i).getVolume());
                float xVal = Float.valueOf(graphList.get(i).getDate().substring(0,4));
                yValues.add(new Entry(xVal, yVal));

//            Toast.makeText(StockViewActivity.this, ""+graphList.get(i).getVolume(), Toast.LENGTH_SHORT).show();
            }
        }


        setUpChart(yValues);
    }

    private void setUpChart(ArrayList<Entry> yValues) {

        // disable description
        mChart.getDescription().setEnabled(false);

        // animate
        mChart.animateX(1500);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        LineDataSet lineDataSet = new LineDataSet(yValues, null);

//        lineDataSet.setFillAlpha(110);

        lineDataSet.setDrawIcons(false);
        lineDataSet.setLineWidth(1f);
        lineDataSet.setCircleRadius(2f);
        lineDataSet.setValueTextSize(9f);
        lineDataSet.setCircleHoleRadius(1f);
        lineDataSet.setColor(Color.BLACK);
        lineDataSet.setCircleColor(Color.BLACK);
        lineDataSet.setDrawValues(true);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawCircleHole(false);
        lineDataSet.enableDashedLine(10f, 5f, 0f);
        lineDataSet.enableDashedHighlightLine(10f, 5f, 0f);
        lineDataSet.setFormLineWidth(1f);
        lineDataSet.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        lineDataSet.setFormSize(15.f);

        if (Utils.getSDKInt() >= 18) {
            Drawable drawable = ContextCompat.getDrawable(StockViewActivity.this, R.drawable.fade_background_gradient);
            lineDataSet.setFillDrawable(drawable);
        } else {
            lineDataSet.setFillColor(Color.BLACK);
        }

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        LineData data = new LineData(dataSets);
        mChart.setData(data);
        mChart.getAxisRight().setEnabled(false);

//        String[] values = new String[]
//                {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
//                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""};

        XAxis xAxis = mChart.getXAxis();
//        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));
        xAxis.setGranularity(1);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.enableGridDashedLine(10f, 10f, 0f);

        YAxis yAxis = mChart.getAxisLeft();
        yAxis.enableGridDashedLine(10f, 10f, 0f);
        yAxis.setDrawZeroLine(false);

        // Setup Legend
        Legend legend = mChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setTextColor(R.color.colorPrimary);
        legend.setDirection(Legend.LegendDirection.RIGHT_TO_LEFT);
    }


}
