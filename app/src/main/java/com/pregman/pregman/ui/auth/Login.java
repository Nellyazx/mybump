package com.pregman.pregman.ui.auth;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pregman.pregman.data.api.ApiService;
import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.utils.ConnectionDetector;
import com.pregman.pregman.utils.MySingleTone;
import com.stockbroker.stockbroker.R;
import com.pregman.pregman.ui.main.StocksActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    TextView signup, forgotpassword;
    FloatingActionButton btnLogin;
    EditText username, password;
    String pickusername, pickpassword;
    AlertDialog.Builder alertDialog;
    StringRequest stringRequest;
    ProgressDialog progressDialog;

    MyDb myDb;
    Boolean isInternetPresent = false;
    ConnectionDetector mConnectionDetector;
    int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        setContentView(R.layout.activity_login);

        myDb = new MyDb(Login.this);
        mConnectionDetector = new ConnectionDetector(Login.this);
        isInternetPresent = mConnectionDetector.isConnectingToInternet();
        alertDialog = new AlertDialog.Builder(Login.this);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
//        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        signup = (TextView) findViewById(R.id.txtsignup);
        //take me to resetpassword
        forgotpassword = (TextView) findViewById(R.id.txtforgotpassword);
        //take me to main activity
        btnLogin = (FloatingActionButton) findViewById(R.id.btnlogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickusername = username.getText().toString();
                pickpassword = password.getText().toString();

                if (pickusername.isEmpty() || pickpassword.isEmpty()) {
                    alertDialog.setTitle("Error");
                    displayAlert("Please Fill in all the Fields");
                } else {
                    getData();
                    MySingleTone.getInstance(Login.this).addToRequestQueue(stringRequest);
                }
            }
        });

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent forgot = new Intent(getApplicationContext(), ForgotPassword.class);
                startActivity(forgot);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signup = new Intent(getApplicationContext(), RegisterUser.class);
                startActivity(signup);
            }
        });
    }

    public void displayAlert(final String message) {
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                username.setText("");
                password.setText("");
            }
        });
        AlertDialog alertDiag = alertDialog.create();
        alertDiag.show();
    }

    public void getData() {
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        if (isInternetPresent) {

            progressDialog = ProgressDialog.show(Login.this, "", "Logging in...", false, false);

            stringRequest = new StringRequest(Request.Method.POST, ApiService.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String code = jsonObject.getString("code");

                    if (code.equals("login_failed")) {
                        alertDialog.setTitle("Login Error");
                        // alertDialog.setMessage(message);
                        displayAlert(jsonObject.getString("message"));
                    } else {

                        myDb.open();

                        myDb.insertUser(jsonObject.getString("name"),jsonObject.getString("email"));

                        myDb.close();

                        startActivity(new Intent(Login.this, StocksActivity.class));


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//                Toast.makeText(Login.this, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", pickusername);
                params.put("password", pickpassword);
                return params;
            }
        };

        } else {
            Toast.makeText(Login.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (count == 0) {
                Toast.makeText(Login.this, "Click again to exit application ", Toast.LENGTH_SHORT).show();
                count++;
            } else {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        return true;
    }
}
