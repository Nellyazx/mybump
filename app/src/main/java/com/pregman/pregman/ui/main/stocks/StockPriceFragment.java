package com.pregman.pregman.ui.main.stocks;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import com.pregman.pregman.data.api.ApiService;
import com.pregman.pregman.data.model.StockInfo;
import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.utils.ConnectionDetector;
import com.pregman.pregman.utils.MySingleTone;

import com.stockbroker.stockbroker.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class StockPriceFragment extends Fragment {
    RecyclerView recyclerView;
    StocksAdapter adapter;
    List<StockInfo> items;
    StringRequest stringRequest;
    MyDb myDb;
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean isInternetPresent = false;
    ConnectionDetector mConnectionDetector;
//    TextView textView;

    public StockPriceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stock_price, container, false);

        myDb = new MyDb(getActivity());
        mConnectionDetector = new ConnectionDetector(getActivity());
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) v.findViewById(R.id.stockPrice);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        recyclerView.addItemDecoration(divider);
        recyclerView.setHasFixedSize(true);

        items = new ArrayList<>();

        getData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPage();
            }
        });

//        textView= (TextView) v.findViewById(R.id.StockName);

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
//                textView.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                refreshPage();
            }
        }.start();

        return v;
    }

    public void refreshPage() {

        adapter = new StocksAdapter(getActivity(), items);
        adapter.clearAdapter();

        getData();
    }


    public void getData() {
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        if (isInternetPresent) {
            swipeRefreshLayout.setRefreshing(true);

            Map<String, String> params = new HashMap<String, String>();
            params.put("rid", "1");
            params.put("pin", "1234");

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiService.GET_STOCKS_URL, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {

                        if (response != null) {

                            JSONArray array = response.getJSONArray("result");
                            items.clear();
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject myObject = array.getJSONObject(i);
                                StockInfo myStock = new StockInfo(
                                        myObject.getString("stock"),
                                        myObject.getDouble("yesterday"),
                                        myObject.getDouble("today"),
                                        myObject.getDouble("high"),
                                        myObject.getDouble("volume")
                                );
                                items.add(myStock);
                            }
                        }

                        swipeRefreshLayout.setRefreshing(false);

                        adapter = new StocksAdapter(getActivity(), items);
                        recyclerView.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            MySingleTone.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

            jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } else {
            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }
}
