package com.pregman.pregman.ui.main.wishlist;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.ui.auth.Login;
import com.pregman.pregman.ui.main.MoreInfo;
import com.stockbroker.stockbroker.R;

public class Contact extends AppCompatActivity {
ProgressDialog progressDialog;
MyDb myDb;
Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Contact Service Providers");
        setSupportActionBar(toolbar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.allergy:
                //  navigate to home stock list page
                Intent intent = new Intent(Contact.this,MoreInfo.class);
                startActivity(intent);
                return true;
            case R.id.contacts:
                //toolbar.setTitle("Market");
                //Toast.makeText(Contact.this, "You Clicked on Contacts", Toast.LENGTH_SHORT).show();
                //  navigate to home stock list page
                Intent intenttwo = new Intent(Contact.this,Contact.class);
                startActivity(intenttwo);
                return true;
            case R.id.classes:
                //  toolbar.setTitle("Watch List");
                Toast.makeText(Contact.this, "You Clicked on Classes", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sixmon:
                Toast.makeText(Contact.this, "You Clicked on six months", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_logout:

                AlertDialog.Builder builder = new AlertDialog.Builder(Contact.this);
                builder.setTitle("Logout");
                builder.setCancelable(false);
                builder.setMessage("Are you sure you want to logout?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog = ProgressDialog.show(Contact.this, "", "Logging out...", false, false);

                        myDb.open();
                        myDb.deleteUser();
                        myDb.close();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                Intent i = new Intent(Contact.this, Login.class);
                                startActivity(i);
                            }
                        }, 2000);
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }



    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

   
}
