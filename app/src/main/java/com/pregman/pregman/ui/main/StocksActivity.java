package com.pregman.pregman.ui.main;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.ui.auth.Login;
import com.pregman.pregman.ui.auth.MapsActivity;
import com.pregman.pregman.ui.main.predictions.PredictPrice;
import com.pregman.pregman.ui.main.wishlist.Contact;
import com.pregman.pregman.ui.main.wishlist.WatchListFragment;
import com.pregman.pregman.utils.BottomNavigationBehavior;
import com.stockbroker.stockbroker.R;
import com.pregman.pregman.ui.main.market.MarketDetailsFragment;

public class StocksActivity extends AppCompatActivity {
    Toolbar toolbar;

    ProgressDialog progressDialog;
    int count = 0;
    MyDb myDb;
//    TabLayout tabLayout;
//    ViewPager viewPager;
//    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_panel);

        myDb = new MyDb(StocksActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       // toolbar.setTitle("Beryl's Timeline");
getSupportActionBar().setTitle("My Timeline");
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

        // load the stock fragment by default
        toolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);
        //loadFragment(new StockPriceFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_stock_price:
                    toolbar.setTitle("Home");
//                    fragment = new StockPriceFragment();
//                    loadFragment(fragment);
                    Intent intent = new Intent(StocksActivity.this,StocksActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_market_details:
                    toolbar.setTitle("Appointment");
                    fragment = new MarketDetailsFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_watch_list:
                    toolbar.setTitle("Google Maps");
                    fragment = new WatchListFragment();
                    loadFragment(fragment);
//                    Intent intente = new Intent(StocksActivity.this,MapsActivity.class);
//                    startActivity(intente);
                    return true;
                case R.id.navigation_prediction:
                    toolbar.setTitle("Chat");
                    fragment = new PredictPrice();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.allergy:
                //  navigate to home stock list page
                Intent intent = new Intent(StocksActivity.this,MoreInfo.class);
                startActivity(intent);
                return true;
            case R.id.contacts:
                //toolbar.setTitle("Market");
                //Toast.makeText(StocksActivity.this, "You Clicked on Contacts", Toast.LENGTH_SHORT).show();
                //  navigate to home stock list page
                Intent intenttwo = new Intent(StocksActivity.this,Contact.class);
                startActivity(intenttwo);
                return true;
            case R.id.classes:
                //  toolbar.setTitle("Watch List");
                Toast.makeText(StocksActivity.this, "You Clicked on Classes", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sixmon:
                Toast.makeText(StocksActivity.this, "You Clicked on six months", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_logout:

                AlertDialog.Builder builder = new AlertDialog.Builder(StocksActivity.this);
                builder.setTitle("Logout");
                builder.setCancelable(false);
                builder.setMessage("Are you sure you want to logout?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog = ProgressDialog.show(StocksActivity.this, "", "Logging out...", false, false);

                        myDb.open();
                        myDb.deleteUser();
                        myDb.close();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                Intent i = new Intent(StocksActivity.this, Login.class);
                                startActivity(i);
                            }
                        }, 2000);
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return true;

        }
            return super.onOptionsItemSelected(item);
        }



    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (count == 0) {
                Toast.makeText(StocksActivity.this, "Click again to exit application ", Toast.LENGTH_SHORT).show();
                count++;
            } else {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        return true;
    }
}
