package com.pregman.pregman.ui.main.market;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pregman.pregman.data.api.ApiService;
import com.pregman.pregman.data.model.MarketInfo;
import com.pregman.pregman.utils.ConnectionDetector;
import com.pregman.pregman.utils.MySingleTone;
import com.stockbroker.stockbroker.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MarketDetailsFragment extends Fragment {
    RecyclerView myrecyclerView;
    MarketRecyclerViewAdapter myadapter;
    List<MarketInfo> myItems;
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean isInternetPresent = false;
    ConnectionDetector mConnectionDetector;

    public MarketDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_market_details, container, false);
       /* mConnectionDetector = new ConnectionDetector(getActivity());
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        myrecyclerView = (RecyclerView) v.findViewById(R.id.marketDetailsRc);
        myrecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        DividerItemDecoration divider = new DividerItemDecoration(myrecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        myrecyclerView.addItemDecoration(divider);
        myrecyclerView.setHasFixedSize(true);

        myItems = new ArrayList<>();

        getData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPage();
            }
        });*/
        return v;

    }
    public void refreshPage() {

        myadapter = new MarketRecyclerViewAdapter(getActivity(), myItems);
        myadapter.clearAdapter();

        getData();
    }
    public void getData() {
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        if (isInternetPresent) {
            swipeRefreshLayout.setRefreshing(true);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, ApiService.MARKET_DETAILS_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray array = jsonObject.getJSONArray("results");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject myObject = array.getJSONObject(i);
                                    MarketInfo myStock = new MarketInfo(
                                            myObject.getString("share_id"),
                                            myObject.getString("share_code"),
                                            myObject.getString("title")
                                    );
                                    myItems.add(myStock);
                                }

                                swipeRefreshLayout.setRefreshing(false);

                                myadapter = new MarketRecyclerViewAdapter(getActivity(), myItems);
                                myrecyclerView.setAdapter(myadapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    swipeRefreshLayout.setRefreshing(false);
                    error.printStackTrace();
                }
            });

            MySingleTone.getInstance(getActivity()).addToRequestQueue(stringRequest);
        }else{
            Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

}
