package com.pregman.pregman.ui.auth;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pregman.pregman.data.api.ApiService;
import com.pregman.pregman.utils.ConnectionDetector;
import com.pregman.pregman.utils.MySingleTone;
import com.stockbroker.stockbroker.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity {
    EditText email, password, confpassword;
    FloatingActionButton signup;
    String pickemail, pickpass, pickconfpass;
    AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;
    Boolean isInternetPresent = false;
    ConnectionDetector mConnectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mConnectionDetector = new ConnectionDetector(ForgotPassword.this);
        isInternetPresent = mConnectionDetector.isConnectingToInternet();
        setContentView(R.layout.activity_forgot_password);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confpassword = (EditText) findViewById(R.id.confirm_password);
        alertDialog = new AlertDialog.Builder(ForgotPassword.this);
        signup = (FloatingActionButton) findViewById(R.id.btnsignup);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get text from editext
                pickemail = email.getText().toString();
                pickpass = password.getText().toString();
                pickconfpass = confpassword.getText().toString();
                //check that all fields are filled
                if (pickemail.equals("")) {
                    alertDialog.setTitle("Something went Wrong...");
                    alertDialog.setMessage("Please Fill in All the Fields...");
                    displayAlert("input_error");
                } else {
                    if (!(pickpass.equals(pickconfpass))) {
                        alertDialog.setTitle("Something went Wrong...");
                        alertDialog.setMessage("Passwords Mismatch...");
                        displayAlert("reg_failed");
                    } else {
                        getData();
                    }
                }

            }
        });

    }

    public void getData() {
        isInternetPresent = mConnectionDetector.isConnectingToInternet();

        if (isInternetPresent) {

            progressDialog = ProgressDialog.show(ForgotPassword.this, "", "Resetting..", false, false);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiService.FORGOT_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    progressDialog.dismiss();

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String code = jsonObject.getString("code");
                        String message = jsonObject.getString("message");
                        alertDialog.setTitle("Server Response");
                        alertDialog.setMessage(message);
                        displayAlert(code);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", pickemail);

                    return params;
                }
            };

            MySingleTone.getInstance(ForgotPassword.this).addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(ForgotPassword.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void displayAlert(final String code) {
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (code.equals("input_error")) {
                    password.setText("");
                    confpassword.setText("");

                } else if (code.equals("reg_success")) {
                    startActivity(new Intent(getApplicationContext(), Login.class));

                } else if (code.equals("reg_failed")) {
                    email.setText("");
                    password.setText("");
                    confpassword.setText("");
                }
            }
        });
        AlertDialog theDialog = alertDialog.create();
        theDialog.show();
    }

}
