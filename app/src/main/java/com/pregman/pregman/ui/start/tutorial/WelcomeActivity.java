package com.pregman.pregman.ui.start.tutorial;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pregman.pregman.data.prefs.PreferenceManager;
import com.pregman.pregman.data.sqlite.MyDb;
import com.pregman.pregman.*;
import com.pregman.pregman.ui.start.SplashActivity;
import com.stockbroker.stockbroker.R;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager mPager;
    private int[] layouts = {R.layout.firstslide, R.layout.secondslide, R.layout.thirdslide, R.layout.fourthslide};
    private MpagerAdapter mpagerAdapter;
    private LinearLayout Dots_layouts;
    private ImageView[] dots;
    private Button Next, Skip;

    MyDb myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (new PreferenceManager(this).checkpreference()) {
            loadHome();
        }

        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_welcome);

        myDb = new MyDb(WelcomeActivity.this);

        mPager = (ViewPager) findViewById(R.id.viewpager);
        mpagerAdapter = new MpagerAdapter(layouts, this);
        mPager.setAdapter(mpagerAdapter);

        Dots_layouts = (LinearLayout) findViewById(R.id.dotslayout);
        createDots(0);
        Next = (Button) findViewById(R.id.btnNext);
        Skip = (Button) findViewById(R.id.btnSkip);

        Next.setOnClickListener(this);
        Skip.setOnClickListener(this);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
                if (position == layouts.length - 1) {
                    Next.setText("Start");
                    Skip.setVisibility(View.INVISIBLE);
                } else {
                    Next.setText("Next");
                    Skip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void createDots(int current_position) {
        if (Dots_layouts != null) {
            Dots_layouts.removeAllViews();
            ;

            dots = new ImageView[layouts.length];
            for (int i = 0; i < layouts.length; i++) {
                dots[i] = new ImageView(this);
                if (i == current_position) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
                } else {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_dots));

                }
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(4, 0, 4, 0);
                Dots_layouts.addView(dots[i], params);

            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                loadNextslide();
                break;
            case R.id.btnSkip:
                loadHome();
                new PreferenceManager(this).writePreference();
                break;

        }
    }

    private void loadHome() {

        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }

    private void loadNextslide() {
        int nextSlide = mPager.getCurrentItem() + 1;

        if (nextSlide < layouts.length) {
            mPager.setCurrentItem(nextSlide);
        } else {
            loadHome();
            new PreferenceManager(this).writePreference();
        }
    }
}
