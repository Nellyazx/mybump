package com.pregman.pregman.ui.main.wishlist;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pregman.pregman.data.model.MyWish;
import com.pregman.pregman.data.sqlite.Constants;
import com.pregman.pregman.data.sqlite.MyDb;
import com.stockbroker.stockbroker.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class WatchListFragment extends Fragment implements OnMapReadyCallback {
    MyDb myDb;
    RecyclerView recyclerView;
    WishesViewAdapter adapter;
    List<MyWish> Wish;
    private GoogleMap mMap;

    public WatchListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_watch_list, container, false);

//        myDb = new MyDb(getActivity());
//
//        recyclerView = (RecyclerView) v.findViewById(R.id.mywatchlist);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
//        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
//        divider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
//        recyclerView.addItemDecoration(divider);
//        recyclerView.setHasFixedSize(true);
//        Wish = new ArrayList<>();
//        getData();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return v;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//
//        super.onViewCreated(view, savedInstanceState);
//       // MapFragment myFragment = (MapFragment)getChildFragmentManager().findFragmentById(R.id.map);
//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
////        mMap = googleMap;
////
////        // Add a marker in Sydney and move the camera
////        LatLng sydney = new LatLng(-34, 151);
////        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Nairobi"));
////        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//    }
/*
    private void getData() {
        Wish.clear();

        myDb.open();

        Cursor cu = myDb.getWishes();
        while (cu.moveToNext()) {
            String stName = cu.getString(cu.getColumnIndex(Constants.STOCK_NAME));
            String stHigh = cu.getString(cu.getColumnIndex(Constants.STOCK_HIGH));
            String stVol = cu.getString(cu.getColumnIndex(Constants.STOCK_VOLUME));

            MyWish myWish = new MyWish();
            myWish.setStockName(stName);
            myWish.setStockHigh(stHigh);
            myWish.setStockVol(stVol);
            // myWish.setRecordDate(dateText);
            // myWish.setStockId(mid);
            Wish.add(myWish);
        }
        cu.close();

        myDb.close();

        //setup adapter
        adapter = new WishesViewAdapter(getActivity(), Wish);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
*/


}
