package com.pregman.pregman.ui.main.market;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pregman.pregman.data.model.MarketInfo;
import com.stockbroker.stockbroker.R;

import java.util.List;

/**
 * Created by NELSON-PC on 2/7/2018.
 */

public class MarketRecyclerViewAdapter extends RecyclerView.Adapter<MarketRecyclerViewAdapter.MarketViewHolder> {
    private Context context;
    public List<MarketInfo> marketInfos;

    public MarketRecyclerViewAdapter(Context context, List<MarketInfo> marketInfos) {
        this.context = context;
        this.marketInfos = marketInfos;

    }

    @Override
    public MarketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.marketcustomcard, parent, false);
        MarketViewHolder stockViewHolder = new MarketViewHolder(view);
        return stockViewHolder;
    }

    @Override
    public void onBindViewHolder(MarketViewHolder holder, int position) {
        final MarketInfo marketInfo = marketInfos.get(position);
        holder.shareCode.setText(marketInfo.getSharecode());
        holder.shareTitle.setText(marketInfo.getSharetitle());
        holder.shareId.setText(marketInfo.getShareId());

    }

    @Override
    public int getItemCount() {
        if (marketInfos != null) {
            return marketInfos.size();
        }
        return 0;
    }

    public void clearAdapter() {
        marketInfos.clear();
        notifyDataSetChanged();
    }

    //view holder class
    public static class MarketViewHolder extends RecyclerView.ViewHolder {
        //  public CardView cardView;
        public TextView shareId;
        public TextView shareCode;
        public TextView shareTitle;

        public MarketViewHolder(View itemView) {
            super(itemView);
            //  cardView=(CardView) itemView.findViewById(R.id.marketDetails);
            shareId = (TextView) itemView.findViewById(R.id.shareId);
            shareCode = (TextView) itemView.findViewById(R.id.shareCode);
            shareTitle = (TextView) itemView.findViewById(R.id.shareTitle);

        }
    }
}
