package com.pregman.pregman.ui.auth;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.pregman.pregman.ui.main.StocksActivity;
import com.stockbroker.stockbroker.R;

public class Baby extends AppCompatActivity {
    FloatingActionButton done;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baby);
        done=(FloatingActionButton)findViewById(R.id.btnsignup);
        done.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getApplicationContext(),StocksActivity.class));
            }
        });
    }
}
