package com.pregman.pregman.data.model;

import java.io.Serializable;

/**
 * Created by NELSON-PC on 2/7/2018.
 */

public class StockInfo
{
    public double stockPriceToday;
    public String stockName;
    public double stockPriceYesterDay;
    public double stockPriceHigh;
    public double stockPriceVol;

    public StockInfo(String stockName, double stockPriceYesterDay, double stockPriceToday, double stockHigh, double stockVol)
    {
        this.stockPriceYesterDay = stockPriceYesterDay;
        this.stockPriceToday = stockPriceToday;
        this.stockName = stockName;
        this.stockPriceHigh=stockHigh;
        this.stockPriceVol=stockVol;
    }

    public double getStockPriceToday()
    {
        return stockPriceToday;
    }

    public String getStockName()
    {
        return stockName;
    }

    public double getStockPriceYesterDay()
    {
        return stockPriceYesterDay;
    }

    public double getStockHigh() {
        return stockPriceHigh;
    }
    public double getStockVol() {
        return stockPriceVol;
    }


}
