package com.pregman.pregman.data.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDbHelper extends SQLiteOpenHelper {

    private String users_table = "CREATE TABLE IF NOT EXISTS " + Constants.TABLE_USERS
            + "(" + Constants.ID + " INTEGER  PRIMARY KEY AUTOINCREMENT, "
            + Constants.USERNAME + " TEXT, " + Constants.EMAIL
            + " TEXT)";

    private String wishes_table = "CREATE TABLE IF NOT EXISTS " + Constants.TABLE_WISHES
            + "(" + Constants.ID + " INTEGER  PRIMARY KEY AUTOINCREMENT, "
            + Constants.STOCK_NAME + " TEXT, " + Constants.STOCK_HIGH
            + " TEXT, " + Constants.STOCK_VOLUME + " TEXT, " + Constants.RECORD_DATE + " TEXT)";

    public MyDbHelper(Context context, String name, CursorFactory factory,int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(users_table);
        db.execSQL(wishes_table);

        db.getPath();

    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

    }

}
