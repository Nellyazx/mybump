package com.pregman.pregman.data.api;

/**
 * Created by Gershon ~ gmte on 20/02/2018.
 */

public class ApiService {

    public static final String BASE_URL = "https://mobtech.co.ke/stockapp/";

    public static final String LOGIN_URL = BASE_URL + "login.php";
    public static final String REGISTER_URL = BASE_URL + "register.php";
    public static final String FORGOT_URL = BASE_URL + "forgotpassword.php";
    public static final String GET_DATA_JSON = BASE_URL + "sixmonthsdata.json";

    public static final String MARKET_DETAILS_URL = "http://mobtech.co.ke/stocks/market.php";

    public static final String GET_STOCKS_URL = "https://mobile.equitybankgroup.com:8443/mfmbs/getStocks";

}
