package com.pregman.pregman.data.model;

/**
 * Created by Admin on 2/10/2018.
 */

public class MarketInfo {
    public String shareId;
    public String sharecode;
    public String sharetitle;
    // public String sharedesc;

    public MarketInfo(String shareId, String sharecode, String sharetitle) {
        this.shareId = shareId;
        this.sharecode = sharecode;
        this.sharetitle = sharetitle;
        // this.sharedesc = sharedesc;
    }


    public String getShareId() {
        return shareId;
    }

    public String getSharecode() {
        return sharecode;
    }

    public String getSharetitle() {
        return sharetitle;
    }

//    public String getSharedesc()
//    {
//        return sharedesc;
//    }

}
