package com.pregman.pregman.data.model;

/**
 * Created by Gershon ~ gmte on 21/02/2018.
 */

public class Graph {
    private String Security;
    private String Volume;
    private String Date;


    public String getSecurity() {
        return Security;
    }

    public void setSecurity(String security) {
        Security = security;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String volume) {
        Volume = volume;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }



//    "ARM": [
//    {
//        "Date": "2006-Sep-11",
//            "Security": "ARM",
//            "Open": "90.00",
//            "High": "94.00",
//            "Low": "85.00",
//            "Close": "89.00",
//            "Average": "89.50",
//            "Split-Adjusted Average": "17.90",
//            "Volume": "80,000"
//    }


}
