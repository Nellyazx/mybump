package com.pregman.pregman.data.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MyDb {

    MyDbHelper helper;
    Context context;
    SQLiteDatabase db;

    public MyDb(Context c) {
        context = c;
        helper = new MyDbHelper(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    public void open() {
        db = helper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /***************
     * users
     ***************/

    public void insertUser(String username, String email) {
        ContentValues cv = new ContentValues();

        cv.put(Constants.USERNAME, username);
        cv.put(Constants.EMAIL, email);

        db.insert(Constants.TABLE_USERS, null, cv);
    }

    public Cursor getUser() {
        Cursor c = db.rawQuery("SELECT DISTINCT username,email FROM table_users", null);
        return c;
    }

    public void deleteUser() {
        db.delete(Constants.TABLE_USERS, null, null);
    }

    /***************
     * wishlist
     ***************/

    public void insertWish(String stockName, String stockHigh, String stockVolume) {

        ContentValues cv = new ContentValues();
        cv.put(Constants.STOCK_NAME, stockName);
        cv.put(Constants.STOCK_HIGH, stockHigh);
        cv.put(Constants.STOCK_VOLUME, stockVolume);
        cv.put(Constants.RECORD_DATE, System.currentTimeMillis());

        db.insert(Constants.TABLE_WISHES, null, cv);
    }

    public Cursor getWishes() {
        Cursor c = db.rawQuery("SELECT DISTINCT stock_name,stock_high,stock_volume,record_date FROM table_wishes", null);
        return c;
    }

    public void deleteWish() {
        db.delete(Constants.TABLE_WISHES, null, null);
    }

    public boolean deleteWishByName(String stockName) {
        return db.delete(Constants.TABLE_WISHES, Constants.STOCK_NAME + "='" + stockName + "'", null) > 0;
    }


}