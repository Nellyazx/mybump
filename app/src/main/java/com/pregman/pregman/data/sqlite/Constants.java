package com.pregman.pregman.data.sqlite;

/**
 * Created by Admin on 2/15/2018.
 */

public class Constants {

    public static final String DATABASE_NAME = "MyDb.db";
    public static final String ID = "_id";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_USERS = "table_users";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";

    public static final String TABLE_WISHES = "table_wishes";
    public static final String STOCK_NAME = "stock_name";
    public static final String STOCK_HIGH = "stock_high";
    public static final String STOCK_VOLUME = "stock_volume";
    public static final String RECORD_DATE = "record_date";
}
