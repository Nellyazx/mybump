package com.pregman.pregman.data.model;

/**
 * Created by Admin on 2/15/2018.
 */

public class MyWish {

    public String recordDate;
    public String stockHigh;
    public String stockVol;
    public String stockName;

    public String getStockName() {
        return stockName;
    }

    public String setStockName(String stockName) {
        this.stockName = stockName;
        return stockName;
    }

    public String getStockHigh() {
        return stockHigh;
    }

    public double setStockHigh(String stockHigh) {
        this.stockHigh = stockHigh;
        return 0;
    }

    public String getStockVol() {
        return stockVol;
    }

    public double setStockVol(String stockVol) {
        this.stockVol = stockVol;
        return 0;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

}
